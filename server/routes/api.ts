import express    = require('express');
import path       = require('path');
import mongoose   = require('mongoose');
mongoose.Promise = global.Promise;  // Mongoose Promise library deprecated
import { config } from '../config/config';
import { PopDAO } from '../../db/models/pop.dao';
import { CartDAO } from '../../db/models/cart.dao';

/* Connect to MongoLab */
let mongodbUri = `mongodb://${config.db.user}:${config.db.password}@${config.db.url}`;
mongoose.connect(mongodbUri, {useMongoClient: true}).then(
  ()    => { console.log("Connected to DB"); },
  (err) => { console.error.bind(console, err); }
);

/* Set up routes */
const router = express.Router();
router.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'api.html'));
});

/* CRUD for Pop Model */
/**********************/
/* CREATE */
router.post('/pop', (req, res) => {
  let newPop = new PopDAO({
    flavor:       req.body['pop']['flavor'],
    price:        req.body['pop']['price'],
    flavor_type:  req.body['pop']['flavor_type']
  });
  newPop.save((err, pop) => {
    if ( err ) {
      console.error(err);
      res.sendStatus(300);
    } else {
      res.send(pop);
    }
  });
});

/* READ */
router.get('/pops', (req, res) => {
  PopDAO.find({}, (err, pops) => {
    if ( err ) {
      console.error(err);
      res.sendStatus(300);
    } else {
      res.send(pops);
    }
  });
});

/* UPDATE */
router.post('/pop/:_id', (req, res) => {
  let query = {_id: req.params['_id']};
  PopDAO.findOneAndUpdate(query, req.body['pop'], (err, pop) => {
    if ( err ) {
      console.error(err);
      res.sendStatus(300);
    } else {
      res.send(pop);
    }
  });
});

/* DELETE */
router.delete('/pop/:_id', (req, res) => {
  let query = {_id: req.params['_id']};
  PopDAO.findOneAndRemove(query, (err, pop) => {
    if ( err ) {
      console.error(err);
      res.sendStatus(300);
    } else {
      res.send(req.body['pop']);
    }
  });
});
/**********************/

/* CRUD for Cart Model */
/**********************/
router.get('/carts', (req, res) => {
  CartDAO.find({}).populate('pops').exec((err, carts) => {
    if ( err ) {
      console.error(err);
      res.sendStatus(300);
    } else {
      res.send(carts);
    }
  });
});
/**********************/

export {router as api};
