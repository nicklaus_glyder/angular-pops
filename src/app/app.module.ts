import { NgModule } from '@angular/core';
// Google Maps, must be in Root module
import { AgmCoreModule } from '@agm/core';
// App Modules
import { CoreModule } from 'app/core/core.module';
import { PopModule } from 'app/pop/pop.module';
import { CartModule } from 'app/cart/cart.module';
// Routing for Root
import { AppRoutingModule } from 'app/app-routing.module';
// Layout and Home Components
import { AppComponent } from 'app/app.component';
import { HomeComponent } from 'app/core/home/home.component';
import { AboutComponent } from 'app/core/about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent
  ],
  imports: [
    CoreModule,
    PopModule,
    CartModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyABsREz3dxbMA4DDM9GAaBytD3aiAe2wi0'
    })
  ],
  entryComponents:  [],
  bootstrap:        [ AppComponent ]
})
export class AppModule {}
