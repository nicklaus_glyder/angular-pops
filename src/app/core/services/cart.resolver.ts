import { Injectable } from '@angular/core';
import { Resolve,
         ActivatedRouteSnapshot,
         RouterStateSnapshot } from '@angular/router';
import { Cart }        from 'app/core/models/cart.model';
import { CartService } from 'app/core/services/cart.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CartResolver implements Resolve<Cart> {
  constructor(private backend: CartService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) : Observable<any>|Promise<any>|any {
    return this.backend.fetchCarts();
  }
}
