import { Injectable } from '@angular/core';
import { Resolve,
         ActivatedRouteSnapshot,
         RouterStateSnapshot } from '@angular/router';
import { Pop }        from 'app/core/models/pop.model';
import { PopService } from 'app/core/services/pop.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PopResolver implements Resolve<Pop> {
  constructor(private backend: PopService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) : Observable<any>|Promise<any>|any {
    return this.backend.fetchPops();
  }
}
