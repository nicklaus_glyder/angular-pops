import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Pop, FLAVOR_TYPE } from 'app/core/models/pop.model';
import { environment } from 'environments/environment';
import { TestPops } from 'app/core/data/test.data';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class PopService {
  /* Cache */
  private static _Pops : Pop[] = environment.production ? null : [
    TestPops.blueberry, TestPops.lemonbasil, TestPops.strawberry
  ];

  constructor(private http: Http) { }

  fetchPops() : Promise<Pop[]> {
    console.log(environment)
    if (PopService._Pops) {
      return Promise.resolve(PopService._Pops);
    } else {
      return this.http.get('api/pops').toPromise().then(
        response => {
          if (response.status == 200) {
            PopService._Pops = response.json() as Pop[];
            return PopService._Pops;
          } else {
            return null;
          }
        });
    }
  }

  savePop(pop: Pop) : Promise<Pop> {
    // Dummy data short circuit
    if (!environment.production) {
      return Promise.resolve(pop);
    }

    return this.http.post(`api/pop/${pop._id}`, {pop: pop}).toPromise().then(
      response => {
        if (response.status == 200)
          return response.json() as Pop;
        else
          return null;
      });
  }

  createPop(pop: Pop) : Promise<Pop> {
    // Dummy data short circuit
    if (!environment.production) {
      PopService._Pops.push(pop);
      return Promise.resolve(pop);
    }

    return this.http.post(`api/pop`, {pop: pop}).toPromise().then(
      response => {
        if (response.status == 200) {
          PopService._Pops.push(response.json() as Pop);
          return response.json() as Pop;
        } else {
          return null;
        }
      });
  }

  deletePop(pop: Pop) : Promise<boolean> {
    // Dummy data short circuit
    if (!environment.production) {
      PopService._Pops.splice(PopService._Pops.indexOf(pop),1);
      console.log(PopService._Pops)
      return Promise.resolve(true);
    }

    return this.http.delete(`api/pop/${pop._id}`).toPromise().then(
      response => {
        if (response.status == 200) {
          PopService._Pops.splice(PopService._Pops.indexOf(pop),1);
          return true;
        } else {
          return false;
        }
      });
  }

  findPopInCache(id: string) : Pop {
    if (PopService._Pops) {
      for (var i = 0; i < PopService._Pops.length; i++) {
        var pop = PopService._Pops[i];
        if ( id === pop._id ) return pop;
      }
    }
    return null;
  }

}
