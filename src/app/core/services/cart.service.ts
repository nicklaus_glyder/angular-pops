import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Cart } from 'app/core/models/cart.model';
import { Pop, FLAVOR_TYPE } from 'app/core/models/pop.model';
import { environment } from 'environments/environment';
import { TestCarts } from 'app/core/data/test.data';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class CartService {
  /* Cache */
  private static _Carts : Cart[] = environment.production ? null : [
    TestCarts.cart1, TestCarts.cart2, TestCarts.cart3
  ];

  constructor(private http: Http) { }

  fetchCarts() : Promise<Cart[]> {
    if (CartService._Carts) {
      return Promise.resolve(CartService._Carts);
    } else {
      return this.http.get('api/carts').toPromise().then(
        response => {
          if (response.status == 200) {
            CartService._Carts = response.json() as Cart[];
            return CartService._Carts;
          } else {
            return null;
          }
        });
    }
  }

}
