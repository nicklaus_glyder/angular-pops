import { NgModule } from '@angular/core';
import {
        MdToolbarModule, MdSidenavModule, MdTabsModule,
        MdGridListModule, MdListModule,
        MdCardModule, MdButtonModule,
        MdIconModule, MdSliderModule,
        MdSlideToggleModule, MdTooltipModule,
        MdProgressSpinnerModule, MdProgressBarModule,
        MdCheckboxModule, MdRadioModule,
        MdInputModule, MdSelectModule, MdDialogModule,
        MdSnackBarModule, MdButtonToggleModule, MdMenuModule
       } from '@angular/material';
import { MdIconRegistry } from '@angular/material';

@NgModule({
  imports: [
    MdToolbarModule, MdSidenavModule, MdTabsModule,
    MdGridListModule, MdListModule,
    MdCardModule, MdButtonModule,
    MdIconModule, MdSliderModule,
    MdSlideToggleModule, MdTooltipModule,
    MdProgressSpinnerModule, MdProgressBarModule,
    MdCheckboxModule, MdRadioModule,
    MdInputModule, MdSelectModule, MdDialogModule,
    MdSnackBarModule, MdButtonToggleModule, MdMenuModule
  ],
  exports: [
    MdToolbarModule, MdSidenavModule, MdTabsModule,
    MdGridListModule, MdListModule,
    MdCardModule, MdButtonModule,
    MdIconModule, MdSliderModule,
    MdSlideToggleModule, MdTooltipModule,
    MdProgressSpinnerModule, MdProgressBarModule,
    MdCheckboxModule, MdRadioModule,
    MdInputModule, MdSelectModule, MdDialogModule,
    MdSnackBarModule, MdButtonToggleModule, MdMenuModule
  ],
  declarations: []
})
export class MaterialModule {}
