import { Directive, Input, TemplateRef,
         ViewContainerRef, EmbeddedViewRef, OnDestroy } from '@angular/core';

@Directive({ selector: '[popIfMediaQuery]'})
export class IfMediaQueryDirective {
  private mql: MediaQueryList = null;
  private mqlisten: (mql: MediaQueryList) => void;

  constructor( private templateRef: TemplateRef<any>,
               private viewContainer: ViewContainerRef ) {}

  @Input() set popIfMediaQuery (query: string) {
    if (!this.mql) {
      this.mql = window.matchMedia(query);
      if(this.mql.matches) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainer.clear();
      }
    }
  }

}
