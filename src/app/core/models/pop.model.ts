export enum FLAVOR_TYPE {
  FRUIT    = "FRUIT",
  MILK     = "MILK",
  COCKTAIL = "COCKTAIL"
};

/* POJSO for use in Angular */
export class Pop {
  _id: string;
  flavor: string;
  price: number;
  flavor_type: FLAVOR_TYPE;

  constructor (flavor: string, price: number, flavor_type: FLAVOR_TYPE) {
    this.flavor = flavor;
    this.price = price;
    this.flavor_type = flavor_type;
  }

  public static compareTo(left: Pop, right: Pop) : Boolean {
    if (   left.flavor == right.flavor
        && left.price == right.price
        && left.flavor_type == right.flavor_type ) {
        return true;
      } else {
        return false;
      }
  }
};
