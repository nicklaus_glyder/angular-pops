import { Pop } from 'app/core/models/pop.model';

export class Cart {
  _id: string;
  address: string;
  lat: number;
  lng: number;
  pops: Pop[];

  constructor (address: string, lat: number, lng: number) {
    this.address = address;
    this.lat = lat;
    this.lng = lng;
  }

  public static compareTo(left: Cart, right: Cart) : boolean {
    if (   left.address == right.address
        && left.lat == right.lat
        && left.lng == right.lng ) {
        return true;
      } else {
        return false;
      }
  }
}
