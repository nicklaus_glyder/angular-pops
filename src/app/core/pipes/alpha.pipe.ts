import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'sortAlphabetical'})
export class AlphaPropertyPipe implements PipeTransform {
  transform(value: {}[], property: string): {}[] {
    return value.sort((a,b) =>
      a[property].localeCompare(b[property].toString())
    );
  }
}
