import { Pop, FLAVOR_TYPE } from 'app/core/models/pop.model';
import { Cart } from 'app/core/models/cart.model';

export const TestPops = {
    blueberry:  <Pop> {flavor: "Blueberry", price: 3.00, flavor_type: FLAVOR_TYPE.FRUIT, _id: "test1"},
    lemonbasil: <Pop> {flavor: "Lemon Basil", price: 2.00, flavor_type: FLAVOR_TYPE.FRUIT, _id: "test2"},
    strawberry: <Pop> {flavor: "Strawberry", price: 3.50, flavor_type: FLAVOR_TYPE.FRUIT, _id: "test3"}
};

export const TestCarts = {
  cart1:  <Cart> {
    _id: "cart1", address: "John's Island",
    lat: 33.000, lng: -79.924899,
    pops: [TestPops.blueberry, TestPops.lemonbasil]
  },
  cart2:  <Cart> {
    _id: "cart2", address: "Drum Island",
    lat: 32.807608, lng: -79.924899,
    pops: [TestPops.blueberry, TestPops.strawberry]
  },
  cart3:  <Cart> {
    _id: "cart3", address: "Atlantis",
    lat: 32.407608, lng: -79.924899,
    pops: [TestPops.blueberry, TestPops.lemonbasil, TestPops.strawberry]
  }
}
