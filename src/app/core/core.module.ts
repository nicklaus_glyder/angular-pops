import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'app/core/material/material.module';
import 'hammerjs';
import { AgmCoreModule } from '@agm/core';

// Pipes
import { AlphaPropertyPipe } from 'app/core/pipes/alpha.pipe';
// Directives
import { IfMediaQueryDirective } from 'app/core/directives/popIfMediaQuery.directive';
// Servies and Resolvers
import { PopService } from 'app/core/services/pop.service';
import { PopResolver } from 'app/core/services/pop.resolver';
import { CartService } from 'app/core/services/cart.service';
import { CartResolver } from 'app/core/services/cart.resolver';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    AgmCoreModule
  ],
  declarations: [
    AlphaPropertyPipe,
    IfMediaQueryDirective
  ],
  providers: [
    PopService, PopResolver,
    CartService, CartResolver
  ],
  exports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    AgmCoreModule,
    AlphaPropertyPipe,
    IfMediaQueryDirective
  ]
})
export class CoreModule { }
