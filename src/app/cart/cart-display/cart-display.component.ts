import { Component, OnInit, Input } from '@angular/core';
import { Cart } from 'app/core/models/cart.model';
import { Pop, FLAVOR_TYPE } from 'app/core/models/pop.model';

@Component({
  selector: 'app-cart-display',
  templateUrl: './cart-display.component.html',
  styleUrls: ['./cart-display.component.scss']
})
export class CartDisplayComponent implements OnInit {
  @Input() cart;

  constructor() {}

  ngOnInit() {}

  public setType(pop: Pop) {
    switch(pop.flavor_type) {
      case FLAVOR_TYPE.FRUIT:     return {color: 'var(--pop-icon-fruit)'};
      case FLAVOR_TYPE.MILK:      return {color: 'var(--pop-icon-milk)'};
      case FLAVOR_TYPE.COCKTAIL:  return {color: 'var(--pop-icon-cocktail)'};
      default:                    return {color: 'inherit'};
    }
  }

}
