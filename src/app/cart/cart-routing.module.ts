import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartResolver } from 'app/core/services/cart.resolver';
import { PopResolver } from 'app/core/services/pop.resolver';
import { CartReadComponent } from './cart-read/cart-read.component';

const routes: Routes = [
  { path: 'carts', pathMatch: 'prefix', children: [
    { path: 'read',
      component: CartReadComponent,
      resolve: { carts: CartResolver, pops: PopResolver },
      pathMatch: 'prefix'},
    { path: '**', redirectTo: 'read'}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartRoutingModule { }
