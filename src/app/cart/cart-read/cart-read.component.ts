import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AgmMap } from '@agm/core';

import { Cart } from 'app/core/models/cart.model';
import { Pop } from 'app/core/models/pop.model';
import { PopService } from 'app/core/services/pop.service';

@Component({
  selector: 'app-cart-read',
  templateUrl: './cart-read.component.html',
  styleUrls: ['./cart-read.component.scss']
})
export class CartReadComponent implements OnInit {
  @ViewChild('map') map: AgmMap;

  center: {lat: number, lng: number} = {lat: 32.7765, lng: -79.9311}
  carts: Cart[] = [];
  pops: Pop[] = [];

  constructor(private route: ActivatedRoute, private popService: PopService) {}

  ngOnInit() {
    this.carts = this.route.snapshot.data['carts'];
    this.pops = this.route.snapshot.data['pops'];

    this.carts.forEach((cart) => {
      cart.pops.forEach((pop, index) => {
        // cart.pops[index] = this.popService.findPopInCache(pop._id)
      })
    });
  }

}
