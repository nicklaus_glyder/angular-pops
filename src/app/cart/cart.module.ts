import { NgModule } from '@angular/core';
import { CoreModule } from 'app/core/core.module';
import { CartReadComponent } from './cart-read/cart-read.component';
import { CartRoutingModule } from './cart-routing.module';
import { CartDisplayComponent } from './cart-display/cart-display.component';

@NgModule({
  imports: [
    CoreModule,
    CartRoutingModule
  ],
  declarations: [ CartReadComponent, CartDisplayComponent ]
})
export class CartModule { }
