import { NgModule } from '@angular/core';
import { CoreModule } from 'app/core/core.module';

import { PopService } from 'app/core/services/pop.service';
import { PopResolver } from 'app/core/services/pop.resolver';
import { PopRoutingModule } from './pop-routing.module';

import { PopReadComponent } from './pop-read/pop-read.component';
import { PopDisplayComponent } from './pop-display/pop-display.component';
import { PopEditComponent } from './pop-edit/pop-edit.component';
import { PopAddComponent } from './pop-add/pop-add.component';

@NgModule({
  imports:      [
    CoreModule,
    PopRoutingModule
  ],
  declarations: [
    PopReadComponent,
    PopDisplayComponent,
    PopEditComponent,
    PopAddComponent
  ],
  providers:    [ ],
  entryComponents: [ PopEditComponent, PopAddComponent ]
})
export class PopModule { }
