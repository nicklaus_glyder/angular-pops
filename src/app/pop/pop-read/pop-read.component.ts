import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef, MdSnackBar} from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Pop } from 'app/core/models/pop.model';
import { PopService } from 'app/core/services/pop.service';
import { PopDisplayComponent } from 'app/pop/pop-display/pop-display.component';
import { PopEditComponent } from 'app/pop/pop-edit/pop-edit.component';
import { PopAddComponent } from 'app/pop/pop-add/pop-add.component';

@Component({
  selector: 'app-pop-read',
  templateUrl: './pop-read.component.html',
  styleUrls: ['./pop-read.component.scss']
})
export class PopReadComponent implements OnInit {
  pops: Pop[];
  /* Keep reference to last sort event to resort if pops change */
  sortOrder: string;

  constructor(private route: ActivatedRoute,
              private dialog: MdDialog,
              private snackbar: MdSnackBar,
              private Pops: PopService) { }

  ngOnInit() {
    this.pops = this.route.snapshot.data['pops'];
    this.sortOrder = 'alpha';
    this.changeSort();
  }

  // Sort order determined by bound variable in template
  public changeSort () {
    if ( this.sortOrder == 'alpha' ) {
      this.pops.sort((a,b) => a.flavor.localeCompare(b.flavor));
    } else if ( this.sortOrder == 'price' ) {
      this.pops.sort((a,b) => a.price - b.price);
    }
  }

  // Check if changes need saved, then call AJAX service
  private editPop(pop: Pop, newPop: Pop) {
    if (!Pop.compareTo(pop, newPop)) {
      pop.flavor = newPop.flavor;
      pop.price = newPop.price;
      pop.flavor_type = newPop.flavor_type;
      this.changeSort();
      this.Pops.savePop(pop).then((editPop) => {
        if (editPop) {
          let snack = this.snackbar.open("Changes Saved!", "", {duration: 2000});
        } else {
          let snack = this.snackbar.open("ERROR - Changes Not Saved!", "", {duration: 2000});
        }
      });
    }
  }

  private addPop(pop: Pop) {
    this.Pops.createPop(pop).then((newPop) => {
      if (newPop) {
        this.changeSort()
        let snack = this.snackbar.open("Pop Added!", "", {duration: 2000});
      } else {
        let snack = this.snackbar.open("ERROR - Pop Not Added!", "", {duration: 2000});
      }
    });
  }

  // Delete given Pop and remove from local cache
  private deletePop(pop: Pop) {
    this.Pops.deletePop(pop).then((success) => {
      if (success) {
        let snack = this.snackbar.open("Pop Deleted!!", "", {duration: 2000});
      } else {
        let snack = this.snackbar.open("ERROR - Pop Not Deleted!", "", {duration: 2000});
      }
    });
  }

  // Takes in display component to toggle selected animation
  public openEditDialog(pop: Pop, element: PopDisplayComponent) {
    element.toggleSelected();
    let dialogRef = this.dialog.open(PopEditComponent, { data: pop });
        dialogRef.afterClosed().subscribe(
          (result) => {
            if (result) {
              if (result.remove) this.deletePop(pop);
              else if (result.pop) this.editPop(pop, result.pop);
            }
          },
          (error) => { console.error(error); },
          () => {
            element.toggleSelected();
            element.changeType();
          }
        );
  }

  public openAddDialog() {
    let dialogRef = this.dialog.open(PopAddComponent);
        dialogRef.afterClosed().subscribe(
          (pop) => { if (pop) this.addPop(pop) },
          (error) => { console.error(error) }
        );
  }

}
