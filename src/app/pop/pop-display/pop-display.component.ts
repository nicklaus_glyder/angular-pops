import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Pop, FLAVOR_TYPE } from 'app/core/models//pop.model';

@Component({
  selector: 'app-pop-display',
  templateUrl: './pop-display.component.html',
  styleUrls: ['./pop-display.component.scss'],
  animations: [
    trigger('popState', [
        state('unselected',   style({
        backgroundColor: '*',
        transform: 'scale(1)'
      })),
        state('selected',   style({
        backgroundColor: 'var(--bg-selected)',
        transform: 'scale(1.1)'
      })),
      transition('unselected => selected', animate('200ms ease-in')),
      transition('selected   => unselected', animate('400ms ease-out'))
    ])
  ]
})
export class PopDisplayComponent implements OnInit {
  @Input() pop: Pop;
  state: string;
  iconStyle: {color: string};

  constructor() { }

  ngOnInit() {
    this.state = 'unselected';
    this.changeType();
  }

  public changeType() {
    switch(this.pop.flavor_type) {
      case FLAVOR_TYPE.FRUIT:     { this.iconStyle = {color: 'var(--pop-icon-fruit)'}; break; }
      case FLAVOR_TYPE.MILK:      { this.iconStyle = {color: 'var(--pop-icon-milk)'}; break; }
      case FLAVOR_TYPE.COCKTAIL:  { this.iconStyle = {color: 'var(--pop-icon-cocktail)'}; break; }
      default:                    { this.iconStyle = {color: 'inherit'}; }
    }
  }

  public toggleSelected() {
    if (this.state == 'unselected') {
      this.state = 'selected';
    } else {
      this.state = 'unselected';
    }
  }

}
