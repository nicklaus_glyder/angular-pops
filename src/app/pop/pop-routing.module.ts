import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopReadComponent } from './pop-read/pop-read.component';
import { PopResolver } from 'app/core/services/pop.resolver';

const routes: Routes = [
  { path: 'pops', pathMatch: 'prefix', children: [
    { path: 'read',
      component: PopReadComponent,
      resolve: { pops: PopResolver },
      pathMatch: 'prefix'},
    { path: '**', redirectTo: 'read'}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PopRoutingModule { }
