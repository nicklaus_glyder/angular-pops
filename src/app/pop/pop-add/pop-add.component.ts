import { Component, OnInit, Inject } from '@angular/core';
import { Pop, FLAVOR_TYPE } from 'app/core/models/pop.model';

@Component({
  selector: 'app-pop-add',
  templateUrl: './pop-add.component.html',
  styleUrls: ['./pop-add.component.scss']
})
export class PopAddComponent implements OnInit {
  pop: Pop;
  types: FLAVOR_TYPE[] = [
    FLAVOR_TYPE.FRUIT,
    FLAVOR_TYPE.MILK,
    FLAVOR_TYPE.COCKTAIL
  ];

  constructor() { }

  ngOnInit() {
    this.pop = new Pop("", 0, FLAVOR_TYPE.FRUIT);
  }

}
