import { Component, OnInit, Inject } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { Pop, FLAVOR_TYPE } from 'app/core/models/pop.model';

@Component({
  selector: 'app-pop-edit',
  templateUrl: './pop-edit.component.html',
  styleUrls: ['./pop-edit.component.scss']
})
export class PopEditComponent implements OnInit {
  pop: Pop;
  popFlavor: string;
  askDelete: boolean = false;
  types: FLAVOR_TYPE[] = [
    FLAVOR_TYPE.FRUIT,
    FLAVOR_TYPE.MILK,
    FLAVOR_TYPE.COCKTAIL
  ];

  constructor(@Inject(MD_DIALOG_DATA) public data: any,
              private ref: MdDialogRef<PopEditComponent>) { }

  ngOnInit() {
    this.pop = new Pop(this.data.flavor, this.data.price, this.data.flavor_type);
    this.popFlavor = this.pop.flavor;
  }

  toggleDelete() {
    this.askDelete = !this.askDelete;
  }

}
