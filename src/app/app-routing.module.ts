import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from 'app/core/home/home.component';
import { AboutComponent } from 'app/core/about/about.component';

const routes: Routes = [
  // Homepage
  { path: 'home', component: HomeComponent, pathMatch: 'prefix' },
  // Sidenav, add these as routeLinks to buttons or using router service
  { path: 'about', component: AboutComponent, outlet: 'leftnav'},
  // Catchall
  { path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
