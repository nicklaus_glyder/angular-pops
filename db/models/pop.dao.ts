import mongoose = require('mongoose');
import { CartDAO } from './cart.dao';

/* Pop Schema and Model for mongoose/mongo backend*/
export const PopSchema = new mongoose.Schema({
  flavor: String,
  price: Number,
  flavor_type: {
    type: String,
    enum: ['FRUIT', 'MILK', 'COCKTAIL'],
    default: 'FRUIT'
  }
});

// PopSchema.pre('remove', (next) => {
//   PopDAO.findOne({_id: this._id}, (err, pop) => {
//     CartDAO.find({}, (err, carts) => {
//       carts.forEach((cart) => {
//         if ( cart.pops.includes(pop) ) {
//           cart.pops.pop(pop);
//           cart.save();
//         }
//       });
//     });
//   });
//   next();
// });

export const PopDAO = mongoose.model("Pop", PopSchema);
