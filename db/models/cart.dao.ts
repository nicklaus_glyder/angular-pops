import mongoose = require('mongoose');

/* Pop Schema and Model for mongoose/mongo backend*/
export const CartSchema = new mongoose.Schema({
  address: String,
  lat: Number,
  lng: Number,
  pops: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Pop' }]
});
export const CartDAO = mongoose.model("Cart", CartSchema);
